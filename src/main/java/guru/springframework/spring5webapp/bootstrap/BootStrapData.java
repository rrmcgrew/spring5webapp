package guru.springframework.spring5webapp.bootstrap;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import guru.springframework.spring5webapp.model.Author;
import guru.springframework.spring5webapp.model.Book;
import guru.springframework.spring5webapp.model.Publisher;
import guru.springframework.spring5webapp.repositories.AuthorRepository;
import guru.springframework.spring5webapp.repositories.BookRepository;
import guru.springframework.spring5webapp.repositories.PublisherRepository;

@Component
public class BootStrapData implements CommandLineRunner {
	
	private final AuthorRepository authorRepo;
	private final BookRepository bookRepo;
	private final PublisherRepository publisherRepo;

	public BootStrapData(AuthorRepository authorRepo, BookRepository bookRepo, PublisherRepository publisherRepo) {
		this.authorRepo = authorRepo;
		this.bookRepo = bookRepo;
		this.publisherRepo = publisherRepo;
	}

	@Override
	public void run(String... args) throws Exception {		
		Publisher random = new Publisher("House of Random", "123 Easy St", "Nowhere", "IL", "48235");		
		Author marty = new Author("Marty", "McFly");
		Book outtaTime = new Book("Outta Time", "1985-1234-OUT");
		
		marty.getBooks().add(outtaTime);
		outtaTime.getAuthors().add(marty);
		random.getBooks().add(outtaTime);
		authorRepo.save(marty);
		bookRepo.save(outtaTime);
		
		Author doc = new Author("Emmett", "Brown");
		Book flux = new Book("Flux Capacitors: Design and Implementation", "some-random-ISBN");
		
		doc.getBooks().add(flux);
		flux.getAuthors().add(doc);
		random.getBooks().add(flux);
		authorRepo.save(doc);
		bookRepo.save(flux);
		
		Book heavy = new Book("Time Travel and Its Heavy Paradoxes", "another-random-ISBN");
		
		heavy.getAuthors().add(doc);
		heavy.getAuthors().add(marty);
		marty.getBooks().add(heavy);
		doc.getBooks().add(heavy);
		random.getBooks().add(heavy);
		bookRepo.save(heavy);
		authorRepo.save(marty);
		authorRepo.save(doc);
		
		publisherRepo.save(random);
		
		System.out.println("bootstrap started");
		System.out.println("number of books: " + bookRepo.count());		
		System.out.println("publishers: " + publisherRepo.findAll());
		
	}

}
