package guru.springframework.spring5webapp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import guru.springframework.spring5webapp.repositories.BookRepository;

@Controller
public class BookController {	
	private final BookRepository books;	
	
	
	public BookController(BookRepository books) {
		this.books = books;
	}

	@RequestMapping("/books")
	public String getBooks(Model model) {
		model.addAttribute("books", books.findAll());
		
		return "books/list";
	}
}
